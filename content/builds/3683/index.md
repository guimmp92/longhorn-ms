---
title: "3683.Lab06_N"
type: build
url: /builds/3683
build_tag: "6.0.3683.0.Lab06_N.020923-1821"
build_arch: [ "x86" ]
build_m: 3
install_date: "2002-09-24"
install_key: "CKY24-Q8QRH-X3KMR-C6BCY-T847Y"
featured_image: overview.png
---

Build 3683 was the first Codename "Longhorn" build to leak publicly on the internet when it leaked on 20th October 2002 as an x86 Professional compile by the xBetas release group. The publicly leaked version is a modified reduced-size ISO, some private collectors have the unmodified x86 Professional compile.

Though an early build, it has a number of unique features. It is the only Milestone 3 build to feature an Avalon-based display properties control panel, out of the box. The display panel has a number of bugs however, and though present in later Milestone 3 builds, few bugs were fixed (indeed, a cursory examination suggests that it might not have been updated enough to ensure compatibility with the rapidly-changing runtime). As such, this build provides a unique opportunity to examine some early working Avalon code. DCE is partially implemented in this build.

[Tips and tricks for this build](/3683-tips-tricks)

### Gallery

{{< gallery 25 "desktop.png" "explorer.png" "overview.png" "contacts-browser.png" >}}